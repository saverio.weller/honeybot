#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: HoneyBot.py
Author: Saverio Weller
Email: weller.saverio@gmail.com
Gitlab: https://gitlab.com/saverio.weller
Description: Telegram bot to manage cowrie honeypot
             and to execute system specific commands
"""

import logging
import subprocess
import yaml

from aiogram import Bot, Dispatcher, executor, types

CONFIG = yaml.safe_load(open("config.yaml", "r"))
BOT = Bot(token=CONFIG["bot"]["token"])
DISPATCHER = Dispatcher(BOT)

# configure logging
logging.basicConfig(level=logging.INFO)


def custom_keyboard():
    """ Provide a custom keyboard with predefined commands. """
    keyboard = types.reply_keyboard.ReplyKeyboardMarkup(resize_keyboard=True)

    for command in ["cowrie", "ssh"]:
        keyboard.row()
        for button in CONFIG[command]["buttons"]:
            keyboard.insert(button)

    return keyboard


async def on_startup(dispatcher):
    """ Send startup notification. """
    await BOT.send_message(
        CONFIG["bot"]["admin_id"],
        CONFIG["bot"]["startup"],
        reply_markup=custom_keyboard()
    )


@DISPATCHER.message_handler(commands=["start"])
async def send_welcome(message: types.Message):
    """ Send a greeting and provide a custom keyboard. """
    await BOT.send_message(
        message.chat.id,
        CONFIG["bot"]["greeting"],
        reply_markup=custom_keyboard()
    )


@DISPATCHER.message_handler(commands=["cowrie"])
async def cowrie_control(message: types.Message):
    """ Handle basic cowrie commands. """
    response = CONFIG["cowrie"]["usage"]

    if message.get_args() in CONFIG["cowrie"]["commands"]:
        response = subprocess.check_output(
            [f"sudo \
                --preserve-env \
                --user {CONFIG['cowrie']['user']} \
                {CONFIG['cowrie']['path']} {message.get_args()}; \
                exit 0"],
            shell=True,
            stderr=subprocess.STDOUT,
            text=True
        )

    await BOT.send_message(message.chat.id, response)


@DISPATCHER.message_handler(commands=["ssh"])
async def ssh_control(message: types.Message):
    """ Handle basic ssh commands. """
    response = CONFIG["ssh"]["usage"]

    if message.get_args() in CONFIG["ssh"]["commands"]:
        response = subprocess.check_output(
            [f"sudo systemctl \
                --output=cat \
                --show-transaction \
                {message.get_args()} \
                sshd.service; \
                exit 0"],
            shell=True,
            stderr=subprocess.STDOUT,
            text=True
        )

    await BOT.send_message(message.chat.id, response)


if __name__ == "__main__":
    executor.start_polling(DISPATCHER, on_startup=on_startup)
