"""
File: aiogram.py
Author: Saverio Weller
Email: weller.saverio@gmail.com
Gitlab: https://gitlab.com/saverio.weller
Description: Send output messages via Telegram Bot API utilizing the
             asynchronous library AIOGram. Which messages are sent is based
             upon an regular expression filter against the event id.
             Attempted logins and executed commands will also be logged (more
             cleanly) for analysis.
             Required packages: aiogram and pygeoip
"""

import cowrie.core.output
import pygeoip
import re

from aiogram import Bot, Dispatcher, executor
from cowrie.core.config import CowrieConfig


class Output(cowrie.core.output.Output):
    """
    Implementation of Cowrie's abstract class to specify further outputs.
    Mandatory methods are: start, stop and write
    """

    def start(self):
        """
        Initialize the output plugin.

        Following entries must be given in cowrie.cfg:
        cmd_file*       : contains all executed commands
        geoip_database* : file to look up country and coordinates
        filter          : regular expression; filters log based upon 'eventid'
        id              : Telegram user id to send the messages to
        log_file*       : contains all login attempts
        token           : Telegram Bot API token

        (* = subject to changed/removed)
        """
        self.cmd_location = CowrieConfig().get('output_aiogram', 'cmd_file')
        self.database = CowrieConfig().get('output_aiogram', 'geoip_database')
        self.filter = CowrieConfig().get('output_aiogram', 'filter')
        self.id = CowrieConfig().get('output_aiogram', 'id')
        self.log_location = CowrieConfig().get('output_aiogram', 'log_file')
        self.token = CowrieConfig().get('output_aiogram', 'token')

    def stop(self):
        """Shut down output plugin. (Mandatory method)"""
        pass

    def write(self, event):
        """
        Prepare to send message and write to logs.

        The logs contain following TAB-spaced information:
        command log       : timestamp, src_ip, command
        login attempt log : timestamp, src_ip, country_code, country_name,
                            latitude, longitude, username and password
        """
        record = pygeoip.GeoIP(
            self.database,
            flags=pygeoip.const.MMAP_CACHE
        ).record_by_addr(event['src_ip'])

        if "cowrie.login" in event["eventid"]:
            log_file = open(self.log_location, "a")
            log_file.write(
                f"{event['timestamp']}\t"
                f"{event['src_ip']}\t"
                f"{record['country_code']}/{record['country_name']}\t"
                f"{record['latitude']}\t"
                f"{record['longitude']}\t"
                f"{event['username']}\t"
                f"{event['password']}\n"
            )
            log_file.close()

        elif "cowrie.command.input" in event["eventid"]:
            cmd_file = open(self.cmd_location, "a")
            cmd_file.write(
                f"{event['timestamp']}\t"
                f"{event['src_ip']}\t"
                f"{event['message']}\n"
            )
            cmd_file.close()

        if re.match(self.filter, event["eventid"]):
            bot = Bot(token=self.token)
            dispatcher = Dispatcher(bot)

            executor.start(dispatcher, self.send_message(bot, event, record))

    async def send_message(self, bot, event, record):
        """
        (subject to be changed)

        @param bot:  Description
        @type  bot:  aiogram.bot.bot.Bot

        @param event:  Description
        @type  event:  Cowrie event

        """
        await bot.send_message(
            self.id,
            f"{event['src_ip']} "
            f"({record['country_code']}/{record['country_name']})\n"
            f"{event['message']}"
        )
