.PHONY: clean freeze lint run

main = HoneyBot.py
cowrie_plugin = cowrie_output_plugin/aiogram.py
modules = requirements.txt
venv = env

$(venv): $(venv)/bin/activate
$(venv)/bin/activate: $(modules)
	test -d $(venv) || python3 -m venv --prompt HoneyBot $(venv)
	. $(venv)/bin/activate; pip install --upgrade pip
	. $(venv)/bin/activate; pip install --upgrade --requirement $(modules)
	touch $(venv)/bin/activate

clean:
	rm --force --recursive $(venv)

freeze: $(venv)
	. $(venv)/bin/activate; pip freeze > $(modules)

lint: $(venv)
	. $(venv)/bin/activate; flake8 $(main) $(cowrie_plugin)

run: $(venv)
	. $(venv)/bin/activate; $(venv)/bin/python $(main)
